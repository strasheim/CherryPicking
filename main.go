package main

import (
	"errors"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/arn"
	"github.com/aws/aws-sdk-go/aws/ec2metadata"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/ssm"
	"golang.org/x/net/context"
)

func init() {
	log.SetFlags(0)
	awsDefaults()
	forcecache = 300 * time.Second
}

var awsInput []interface{}
var sess []*session.Session
var sizeCache []uint64
var contentCache [][]byte
var forcecache time.Duration

func main() {
	flag.Var(&mounts, "aws", "aws resources to fuse, can be used many times")
	flag.Var(&links, "link", "local symlink list")
	flag.DurationVar(&cachetime, "refresh", 0*time.Minute, "sets the refresh interval")
	flag.StringVar(&mountpoint, "mount", "/tmp/CherryPicking", "setting the mountpoint")
	flag.IntVar(&permissions, "perms", 0444, "file permissions for all mounted files")
	flag.DurationVar(&deadline, "deadline", 200*time.Millisecond, "how long to wait before taking the cached data")
	flag.Usage = usage
	flag.Parse()

	if _, err := os.Stat(mountpoint); os.IsNotExist(err) {
		log.Println("Trying to create missing mountpoint")
		err := os.Mkdir(mountpoint, 0555)
		if err != nil {
			log.Fatalln(err)
		}
	}

	buildcaching()
	forcecache = deadline

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		log.Println("Received term signal, unmounting .. ")
		fuse.Unmount(mountpoint)
	}()

	c, err := fuse.Mount(
		mountpoint,
		fuse.FSName("AWSObject"),
		fuse.Subtype("awsfs"),
		fuse.ReadOnly(),
		fuse.AsyncRead(),
		fuse.LocalVolume(),
		fuse.AllowOther(),
		fuse.DefaultPermissions(),
		fuse.VolumeName(mountpoint),
		fuse.AllowNonEmptyMount(),
	)
	if err != nil {
		log.Fatal(err)
	}
	defer c.Close()

	if cachetime != 0 {
		go refresher()
	} else {
		log.Println("Auto refresh disabled")
	}

	err = fs.Serve(c, FS{})
	if err != nil {
		log.Fatal(err)
	}

	// check if the mount process has an error to report
	<-c.Ready
	if err := c.MountError; err != nil {
		log.Fatal(err)
	}

}

// This is to be called before we serve anything as FS
func buildcaching() {
	for _, object := range mounts {
		log.Println("Caching:", object)
		session, s3 := buildSession(object)
		sess = append(sess, session)
		awsInput = append(awsInput, s3)

		// Now building the cached objects
		data, err := Object(object)
		if err != nil {
			log.Panic(err)
		}
		sizeCache = append(sizeCache, uint64(len(data)))
		contentCache = append(contentCache, data)
	}
}

func refresher() {
	tick := time.Tick(cachetime)
	for {
		select {
		case <-tick:
			// On every tick update the cache
			for index := range mounts {
				go updateCache(index)
			}

		}
	}
}

func updateCache(inode int) {
	log.Println("Updating Cache for:", mounts.Value(inode))
	data, err := Object(mounts.Value(inode))
	if err != nil {
		log.Println("Failed to update the cache for:", mounts.Value(inode), err)
		return
	}
	sizeCache[inode] = uint64(len(data))
	contentCache[inode] = data
}

// This needs to be able to check what object is created
func buildSession(resource string) (sess *session.Session, input interface{}) {

	myArn, err := arn.Parse(resource)
	if err != nil {
		log.Fatalln("ARN:", resource, err)
	}

	sess = session.Must(session.NewSession(&aws.Config{
		Region: aws.String(myArn.Region),
	}))

	switch myArn.Service {
	case "ssm":
		input = &ssm.GetParameterInput{
			Name:           aws.String(strings.Replace(myArn.Resource, "parameter", "", 1)),
			WithDecryption: aws.Bool(true),
		}
	case "lambda":
		input = &lambda.InvokeInput{
			FunctionName: aws.String(myArn.Resource),
		}
	case "s3":
		var tokens []string
		parts := strings.Split(myArn.Resource, "/")
		tokens = append(tokens, parts[0])
		tokens = append(tokens, strings.Replace(myArn.Resource, tokens[0]+"/", "", 1))
		input = &s3.GetObjectInput{
			Bucket: aws.String(tokens[0]),
			Key:    aws.String(tokens[1]),
		}
	default:
		log.Fatalln("Unsupported ARN/resource")
	}
	return sess, input
}

// Object is a generic function that hides the underlying difference of the objects
func Object(object string) ([]byte, error) {
	switch awsInput[mounts.Index(object)].(type) {
	case *s3.GetObjectInput:
		return s3Object(object)
	case *ssm.GetParameterInput:
		return ssmObject(object)
	case *lambda.InvokeInput:
		return lambdaObject(object)
	}
	return nil, errors.New("Unknown Object")
}

func s3Object(object string) ([]byte, error) {
	session := sess[mounts.Index(object)]
	input := awsInput[mounts.Index(object)].(*s3.GetObjectInput)

	svc := s3.New(session)
	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(forcecache))
	defer cancel()
	result, err := svc.GetObjectWithContext(ctx, input, request.WithAppendUserAgent("CherryPicking"))
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(result.Body)
}

func ssmObject(object string) ([]byte, error) {
	session := sess[mounts.Index(object)]
	input := awsInput[mounts.Index(object)].(*ssm.GetParameterInput)

	svc := ssm.New(session)
	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(forcecache))
	defer cancel()
	resp, err := svc.GetParameterWithContext(ctx, input, request.WithAppendUserAgent("CherryPicking"))
	if err != nil {
		return nil, err
	}
	return []byte(*resp.Parameter.Value), nil
}

func lambdaObject(object string) ([]byte, error) {
	session := sess[mounts.Index(object)]
	input := awsInput[mounts.Index(object)].(*lambda.InvokeInput)

	svc := lambda.New(session)
	ctx, cancel := context.WithDeadline(context.Background(), time.Now().Add(forcecache))
	defer cancel()
	result, err := svc.InvokeWithContext(ctx, input, request.WithAppendUserAgent("CherryPicking"))
	if err != nil {
		return nil, err
	}
	return result.Payload, err
}

// Extracting the filename from the ARN
func fileName(info string) string {
	myArn, err := arn.Parse(info)
	if err != nil {
		// If filename is called on an already shorted string
		return info
	}
	switch myArn.Service {
	case "ssm", "s3":
		parts := strings.Split(myArn.Resource, "/")
		return parts[len(parts)-1]
	case "lambda":
		parts := strings.Split(myArn.Resource, ":")
		return parts[len(parts)-1]
	default:
		panic("unknown arn:" + info)
	}

}

func createSymlinks() {
	for index, object := range mounts {
		if len(links)-1 >= index {
			realfile := mountpoint + "/" + fileName(object)
			log.Println("Creating Symlink:", realfile, links[index])
			err := os.Symlink(realfile, links[index])
			if err != nil && !os.IsExist(err) {
				log.Println(err)
			}
		}
	}
}

func awsDefaults() {
	sess := session.Must(session.NewSession())
	svc := ec2metadata.New(sess)

	if !svc.Available() {
		log.Println("No access to AWS metadata")
		return
	}
	data, err := svc.IAMInfo()
	if err != nil {
		log.Println("No IAM information found in AWS metadata")
		return
	}

	details, err := arn.Parse(data.InstanceProfileArn)
	if err != nil {
		log.Println("ARN parsing for defails failed", err)
		return
	}

	// Setting defaults
	defaultRegion = details.Region
	defaultAccount = details.AccountID
	defaultPartition = details.Partition
	goodDefaults = true
}
