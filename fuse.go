package main

import (
	"os"

	"bazil.org/fuse"
	"bazil.org/fuse/fs"
	"golang.org/x/net/context"
)

// FS implements both node and Handle for the root FS, imported from fuse.
type FS struct{}

// Root is the magic mount point, returns fixed the dir below, imported from fuse
func (FS) Root() (fs.Node, error) {
	createSymlinks()
	return Dir{}, nil
}

// Dir implements both Node and Handle for the root directory.
type Dir struct{}

// Attr is required by the fuse API
func (Dir) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = uint64(len(mounts))
	a.Mode = os.ModeDir | 0555
	return nil
}

// Lookup is required by the fuse API
func (Dir) Lookup(ctx context.Context, name string) (fs.Node, error) {
	if mounts.Contains(name) {
		return File{name: name}, nil
	}
	return nil, fuse.ENOENT
}

// ReadDirAll gives back all AWS mounted resources
func (Dir) ReadDirAll(ctx context.Context) ([]fuse.Dirent, error) {
	var overview []fuse.Dirent

	for inode, url := range mounts {
		overview = append(overview, fuse.Dirent{Inode: uint64(inode), Name: fileName(url), Type: fuse.DT_File})
	}
	return overview, nil
}

// File implements both Node and Handle for the hello file.
type File struct {
	name string
}

// Attr is required by the File interface
func (obj File) Attr(ctx context.Context, a *fuse.Attr) error {
	a.Inode = mounts.Inode(obj.name)
	a.Mode = os.FileMode(permissions)
	a.Size = sizeCache[mounts.Index(obj.name)]
	return nil
}

// ReadAll returns the full content of any of the AWS objects
func (obj File) ReadAll(ctx context.Context) ([]byte, error) {
	content, err := Object(obj.name)
	if err != nil {
		if sizeCache[mounts.Index(obj.name)] != 0 {
			return contentCache[mounts.Index(obj.name)], nil
		}
		return nil, err
	}
	return content, err
}
