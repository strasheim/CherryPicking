Please use issues or MR/PRs to add your code to the project. 
Any contribution needs to be without copyright claim. 
Non-technical information should be worded in way that a 
future you can read it without feeling awkward.
