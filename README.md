
Cherry picking, suppressing evidence, or the fallacy of incomplete evidence is the act of pointing to individual cases or data that seem to confirm a particular position while ignoring a significant portion of related cases or data that may contradict that position. It is a kind of fallacy of selective attention, the most common example of which is the confirmation bias. Cherry picking may be committed intentionally or unintentionally. This fallacy is a major problem in public debate. (Wikipedia)


#### Version 1 

1. s3/ssm/lambda object as read only mount
  1. Mounts as read only 0444 
  1. Mount point 0555 
  1. Objects are cached and if a resource become unavailable is served from cache

The filenames need to be unique - basic unix filesystem constrain, This makes enforces that the last part of what is declared to be cherry picked needs to be unique. 

Resoures must be given as AWS ARNs. S3 objects need to include the region as well, which normally can be obmitted. 

### Options 

* -mount [ mountpoint, defaults to /tmp/CherryPicking ]
* -aws [ arn, can be used multiple time ] 
* -link [ path to where you want a link of the -aws to be. It's matched by index[#] ]
* -perms [ permissions of the mounted files, if 0444 is too open. - files are always mounted read only regardless of mode set here ]
* -deadline [ default 200ms, this is not used for the initial cache building ]
* -refresh [ default, no refresh. If set the cache is updated in the background at that interval ]

### MetaInfo 

When this is run on an instance in AWS that has access to the aws metainfo it will set defaults for values not present in the ARN. 

**arn:partition:service:region:account-id:resource**

* partition is set when not present. Currently AWS has: aws, aws-gc, aws-cn
* region is set when not present
* account is set when not present 

The Meta check has a 5 second timeout, which will cause the binary to wait for 5 seconds outside AWS.

### Little things

* The mountpoint is created if no present. Not the full path only the last directory.
* The symlinks are not chmod'd to the permissions defined 


---- 

Information around the core library and technical notes

* https://blog.gopheracademy.com/advent-2014/fuse-zipfs/

There is only a very limited amount on nodefault libraries used from the OO world. 

* golang aws SDK
* golang fuse 
* The downloaded zip is having a statically compiled, compressed binary 
