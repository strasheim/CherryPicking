package main

import (
	"flag"
	"time"

	"github.com/aws/aws-sdk-go/aws/arn"
)

type awsPoints []string

func (i *awsPoints) Set(value string) error {
	// infusing defaults here
	if goodDefaults {
		input, err := arn.Parse(value)
		if err != nil {
			*i = append(*i, value)
			return nil
		}
		var newArn string
		if len(input.Partition) == 0 {
			newArn = "arn:" + defaultPartition + ":" + input.Service
		} else {
			newArn = "arn:" + input.Partition + ":" + input.Service
		}
		if len(input.Region) == 0 {
			newArn += ":" + defaultRegion
		} else {
			newArn += ":" + input.Region
		}
		if len(input.AccountID) == 0 {
			newArn += ":" + defaultAccount
		} else {
			newArn += ":" + input.AccountID
		}
		newArn += ":" + input.Resource
		*i = append(*i, newArn)
		return nil
	}

	*i = append(*i, value)
	return nil
}

func (i *awsPoints) String() string {
	// required by the flags interface
	return "should not be called"
}

func (i *awsPoints) Value(val int) string {
	for j, v := range *i {
		if j == val {
			return v
		}
	}
	panic("Content not found")
}

func (i *awsPoints) Contains(value string) bool {
	for _, v := range *i {
		if fileName(v) == fileName(value) {
			return true
		}
	}
	return false
}

func (i *awsPoints) Inode(value string) uint64 {
	for inode, v := range *i {
		if fileName(v) == fileName(value) {
			return uint64(inode)
		}
	}
	panic("Inode not found")
}

func (i *awsPoints) Index(value string) int {
	for inode, v := range *i {
		if fileName(v) == fileName(value) {
			return inode
		}
	}
	panic("Index not found")
}

func usage() {
	flag.PrintDefaults()
}

var mounts awsPoints
var links awsPoints
var cachetime time.Duration
var deadline time.Duration
var mountpoint string
var permissions int

var goodDefaults bool
var defaultRegion string
var defaultPartition string
var defaultAccount string
